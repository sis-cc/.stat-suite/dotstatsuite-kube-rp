# Kube-rp

`kube-rp` is a development kubernetes cluster linked with many gitops pipelines.

Main services are:

- [sdmx-faceted-search](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-sdmx-faceted-search): NodeJS server with integrated services arround solr
- [proxy](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-proxy): service router to access clustered apps from external world
- [config](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config): centralized http configuration bucket
- [share](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-share)

Main resources:

- solr: containerized solr server
- mongoDB: mongoDB document oriented database

Its target is to host developements arround those services and resources.

It's mainly organized in 2 namespaces: `staging` and `qa`

Applications built from #develop branches are pushed on `staging` whereas those comming from #master are pushed to `qa`

## Architecture

Each namespace follow this schema, except that `qa` may instanciate many replicas of each service to better fit production environnement.

![archi](./docs/archi.png)

# Kube Tips

## Official Tips

https://kubernetes.io/docs/reference/kubectl/cheatsheet/

## Link GKE with gitlab

https://medium.com/john-lewis-software-engineering/deploying-to-google-kubernetes-engine-from-gitlab-ci-feaf51dae0c1

## Load Balancer Setup

### Create global static IP

$ gcloud compute addresses create oecd-qa-static-ip --global

Now use it like:

```
# cat ingress.yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: ingress
  namespace: qa
  annotations:
    kubernetes.io/ingress.global-static-ip-name: "oecd-qa-static-ip"
spec:
  backend:
    serviceName: proxy
    servicePort: 80
```

### DNS Entries

Create a entry for each namespace : `qa.oecd.siscc.org => <oecd-qa-static-ip>`

Create an alias per application:

```
webapp.staging.oecd.siscc.org. => oecd.siscc.org.
```

### HTTPS

#### Create self signed certificate (dev only)

```
$ mkdir qa/certs && cd qa/certs
$ openssl genrsa -out qa-ingress-dx.key 2048
$ openssl req -new -key qa-ingress-dx.key -out qa-ingress-dx.csr -subj "/CN=data-explorer.qa.oecd.siscc.org"
$ openssl x509 -req -days 365 -in qa-ingress-dx.csr -signkey qa-ingress-dx.key -out qa-ingress-dx.crt
```

#### Create kube secrets

```
$ kubectl create secret tls dx-qa-cert-secret --cert qa-ingress-dx.crt --key qa-ingress-dx.key -n qa
```

#### Update ingress manifest

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: ingress
  namespace: qa
  annotations:
    kubernetes.io/ingress.global-static-ip-name: "oecd-qa-static-ip"
    # ingress.gcp.kubernetes.io/pre-shared-cert: "webapp-qa-cert, data-explorer-qa-cert"
spec:
  tls:
  - secretName: data-explorer-qa-cert-secret
  - secretName: webapp-qa-cert-secret
  backend:
    serviceName: proxy
    servicePort: 80
```

```
$ kubectl apply -f ingress.yaml
$ kubectl describe ingress ingress -n qa
Name:             ingress
Namespace:        qa
Address:          35.186.222.13
Default backend:  proxy:80 (10.16.2.212:80)
TLS:
  data-explorer-qa-cert-secret terminates
  webapp-qa-cert-secret terminates
Rules:
  Host  Path  Backends
  ----  ----  --------
  *     *     proxy:80 (10.16.2.212:80)
Annotations:
  ingress.kubernetes.io/forwarding-rule:             k8s-fw-qa-ingress--2bf6031437a30d3f
  ingress.kubernetes.io/https-target-proxy:          k8s-tps-qa-ingress--2bf6031437a30d3f
  ingress.kubernetes.io/target-proxy:                k8s-tp-qa-ingress--2bf6031437a30d3f
  ingress.kubernetes.io/backends:                    {"k8s-be-30145--2bf6031437a30d3f":"HEALTHY"}
  ingress.kubernetes.io/ssl-cert:                    k8s-ssl-7ace411a7775b55b-32ddc7cab941b38d--2bf6031437a30d3f,k8s-ssl-7ace411a7775b55b-316437af34755a14--2bf6031437a30d3f
  ingress.kubernetes.io/url-map:                     k8s-um-qa-ingress--2bf6031437a30d3f
  kubectl.kubernetes.io/last-applied-configuration:  {"apiVersion":"extensions/v1beta1","kind":"Ingress","metadata":{"annotations":{"kubernetes.io/ingress.global-static-ip-name":"oecd-qa-static-ip"},"name":"ingress","namespace":"qa"},"spec":{"backend":{"servic
eName":"proxy","servicePort":80},"tls":[{"secretName":"data-explorer-qa-cert-secret"},{"secretName":"webapp-qa-cert-secret"}]}}
  kubernetes.io/ingress.global-static-ip-name:  oecd-qa-static-ip
  ingress.kubernetes.io/https-forwarding-rule:  k8s-fws-qa-ingress--2bf6031437a30d3f
Events:
  Type    Reason  Age   From                     Message
  ----    ------  ----  ----                     -------
  Normal  ADD     20m   loadbalancer-controller  qa/ingress
  Normal  CREATE  19m   loadbalancer-controller  ip: 35.186.222.13
```

## Add hub.docker secret

Add secret for each namespace

```
$ kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v1/ --docker-username=ebasley --docker-password=<password> --docker-email=<email> --namespace=default
```

Then add secret in deployment manifests:

```
spec:
      imagePullSecrets:
      - name: regcred
      containers:
      - name: proxy
```
