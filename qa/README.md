# KUBE QA

## dive in
```
gcloud config set project oecd-228113
gcloud config set container/cluster oecd
gcloud config set compute/region europe-west1
gcloud container clusters get-credentials oecd --region europe-west1-b
kubectl get pods -n qa -o wide
```

## proxy
routes are mounted during deployment in the proxy service from a kube configmap named proxy-routes:
`kubectl create configmap -n qa proxy-routes --from-file=routes.json=./proxy/routes.json`

---

## solr
1. apply solr init `kubectl apply -f solr.init.yaml`
1. enter the container `kubectl exec -it solr-<hash> -n qa -- bash`
1. create the core `solr create sdmx-facet-search`
1. move the solr file `mv opt/solr/server/solr/*  /data`
1. note: /data should contains files and at least solr.xml
1. remove the worker
1. apply solr `kubectl apply -f solr.yaml`

## keycloak with helm
> see keycloak.md (at root level)
