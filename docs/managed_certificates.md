## HTTPS

### Managed certificates

It's still an alpha world, I was unable to find a satisfactory solution, but works on 3 ways to use managed certificates with GCE ingress

1. create gce certificates and associate them manually
2. create gce certificates manually and declare them in ingress manifest
3. generate managed certificate


##  Create gce certificates and associate them manually

```
$ gcloud beta compute ssl-certificates create "oecd-cert" --domains oecd.redpelicans.com
$ gcloud compute url-maps list
NAME                                      DEFAULT_SERVICE
k8s-um-default-ingress--2bf6031437a30d3f  backendServices/k8s-be-32392--2bf6031437a30d3f
$ gcloud compute target-https-proxies create https-target --url-map=k8s-um-default-ingress--2bf6031437a30d3f --ssl-certificates=oecd-cert
$ gcloud compute addresses list
NAME                    ADDRESS/RANGE  TYPE  PURPOSE  NETWORK  REGION  SUBNET  STATUS
oecd-staging-static-ip  35.244.225.85                                          IN_USE
$ gcloud compute forwarding-rules create https-global-forwarding-rule --global --ip-protocol=TCP --ports=443 --target-https-proxy=https-target --address oecd-staging-static-ip
$ watch gcloud beta compute ssl-certificates list
NAME       TYPE     CREATION_TIMESTAMP             EXPIRE_TIME                    MANAGED_STATUS
oecd-cert  MANAGED  2019-01-17T02:05:26.353-08:00  2019-04-17T02:16:42.000-07:00  ACTIVE
    oecd.redpelicans.com: ACTIVE
# Add following annotation in proxy.yaml
annotations:
    ingress.kubernetes.io/target-proxy: https-target
$ kubectl apply -f proxy.yaml
```

Work with one certificate, should trying to use more ...

## Create gce certificates manually and declare them in ingress manifest

As before create a managed certificate in gcloud, then refer to it via an ingress annotation:

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: ingress
  namespace: default
  annotations:
    kubernetes.io/ingress.global-static-ip-name: "oecd-staging-static-ip"
    ingress.gcp.kubernetes.io/pre-shared-cert: "oecd-cert, oecd-cert1"
    ...
```

Failed ...

see: https://cloud.google.com/kubernetes-engine/docs/how-to/ingress-multi-ssl

3. Generate managed certificate

Never tested ...

see: https://cert-manager.readthedocs.io/en/latest/index.html
