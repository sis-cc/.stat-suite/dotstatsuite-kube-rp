# dive in

```
gcloud config set project oecd-228113
gcloud config set container/cluster oecd
gcloud config set compute/region europe-west1
gcloud container clusters get-credentials oecd --region europe-west1-b
kubectl get pods -n staging -o wide
```

# setup

1. kubectl create -f namespace.json
1. create docker regcred secret from `regcred.sh`
1. create secrets from `secrets.sh`

> NO KEYCLOAK IN STAGING because qa frontend use staging backend, keycloak is in-between

## keycloak with helm

> see keycloak.md (at root level)

# solr

1. apply solr init `kubectl apply -f solr.init.yaml`
1. enter the container `kubectl exec -it solr-<hash> -n qa -- bash`
1. create the core `solr create sdmx-facet-search`
1. move the solr file `mv opt/solr/server/solr/*  /data`
1. note: /data should contains files and at least solr.xml
1. remove the worker
1. apply solr `kubectl apply -f solr.yaml`

# sfs

- curl -X DELETE https://sfs-qa.siscc.org/admin/dataflows?api-key=<key> | jq
- curl -X DELETE https://sfs-qa.siscc.org/admin/config?api-key=<key> | jq
- curl -X POST https://sfs-qa.siscc.org/admin/dataflows?api-key=<key> -d {} | jq
- curl -X GET https://sfs-qa.siscc.org/admin/report?api-key=<key> | jq
